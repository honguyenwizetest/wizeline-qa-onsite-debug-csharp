﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.PageObjects;
using System;

namespace TakeHomeTest
{
    public class StepDefs
    {
        IWebDriver driver;

        By byEmail = By.Name("email");
        By invalidEmailErr = By.Id("emailError");

        [FindsBy(How = How.Id, Using = "user_email")]
        private IWebElement inputUsername;

        [FindsBy(How = How.Id, Using = "user_password")]
        private IWebElement inputPassword;

        [FindsBy(How = How.XPath, Using = "//input[contains(@class,'btn-default')]")]
        private IWebElement btnLogin;

        [FindsBy(How = How.ClassName, Using = "panel-body")]
        private IWebElement divLoginMessage;

        [FindsBy(How = How.XPath, Using = "//ul[@id='myTab']/li/a[contains(@href,'api')]")]
        private IWebElement linkApiMemu;

        [FindsBy(How = How.Name, Using = "api_key_form[name]")]
        private IWebElement txtAPIName;

        [FindsBy(How = How.Name, Using = "commit")]
        private IWebElement btnGenerateAPI;

        [FindsBy(How = How.XPath, Using = "/html/body/div[3]/div[3]/div[3]/div[1]/table/tbody/tr[22]/td[3]/a[2]")]
        private IWebElement btnLastDeleteAPIbutton;

        [FindsBy(How = How.CssSelector, Using = ".panel-body")]
        private IWebElement divAPIStatusMessage;


        [SetUp]
        public void startBrowser()
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Url = "https://home.openweathermap.org/users/sign_in";
            PageFactory.InitElements(driver, this);
        }

        [Test]
        public void LoginSuccessfully()
        {
            inputUsernamePassword("wizetest1@gmail.com", "autotest");
            Login();
            Assert.AreEqual(GetLoginMessage(), "Signed in successfully.");
        }

        [Test]
        public void addNewAPIKey()
        {
            String apiName = "myAPIkey";
            LoginSuccessfully();
            OpenAPIKeyMenu();

            GenerateAPI(apiName);
            Assert.AreEqual(divAPIStatusMessage.Text, "API key was created successfully");
        }

        [Test]
        public void removeTheLastAPIKey()
        {
            LoginSuccessfully();
            OpenAPIKeyMenu();

            DeleteLastAPI();
            Assert.AreEqual(divAPIStatusMessage.Text, "API key was deleted successfully");
        }

        public void inputUsernamePassword(String username, String pass)
        {
            inputUsername.SendKeys(username);
            inputPassword.SendKeys(pass);
        }

        public void Login()
        {
            btnLogin.Click();
        }

        public String GetLoginMessage()
        {
            return divLoginMessage.Text;
        }

        public void OpenAPIKeyMenu()
        {
            linkApiMemu.Click();
        }

        public void GenerateAPI(String apiName)
        {
            txtAPIName.SendKeys(apiName);
            btnGenerateAPI.Click();
        }

        public void DeleteLastAPI()
        {
            btnLastDeleteAPIbutton.Click();
        }
    }
}
